#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


# create a global dictionary for caching cycle numbers 
collatz_dict = {}

def collatz_eval_helper(x):
    current_cycle = 1
    while x != 1: 
        if x in collatz_dict:  
            current_cycle = current_cycle + collatz_dict[x] - 1
            break
        elif x % 2 == 0: 
            x = x // 2
        else: 
            x = (3 * x) + 1 
        current_cycle += 1
    return current_cycle

def collatz_eval(i, j):
    global collatz_dict
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    max_num = 1 
    # go through all of the numbers within the range 
    for x in range(min(i, j),max(j, i) + 1):
        temp = x
        if temp in collatz_dict:
            current_cycle = collatz_dict[temp]
        elif temp not in collatz_dict:
            current_cycle = collatz_eval_helper(x)
            collatz_dict[temp] = current_cycle  
        if max_num < current_cycle: 
            max_num = current_cycle
    return max_num

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
